import mongoose from 'mongoose';

import { PersonsModel } from '../models/persons.model';

export class PersonsService {
  async getAll() {
    return await PersonsModel.find()
  }

  async create(person: any) {
    await PersonsModel.create(person)
  }

  async deleteOne(id: string) {
    await PersonsModel.deleteOne({ _id: mongoose.Types.ObjectId(id) }).exec();
  }
}
