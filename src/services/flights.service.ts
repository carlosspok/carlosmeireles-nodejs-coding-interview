import { NotFoundError } from 'routing-controllers';

import { AddPassengerDto } from '../controllers/dto';
import { FlightsModel } from '../models/flights.model';
import { PersonsModel } from '../models/persons.model';

export class FlightsService {
  async getAll() {
    return await FlightsModel.find().exec()
  }

  async addPassenger(addPassengerDto: AddPassengerDto) {
    const flight = await FlightsModel.findOne({ code: addPassengerDto.code }).exec()

    if (!flight)
      throw new NotFoundError(`Flight doesn't exit.`);

    const person = await PersonsModel.findOne({ email: addPassengerDto.email }).exec()

    if (!person)
      throw new NotFoundError(`Passenger doesn't exit.`);

    if (!flight.passengers) flight.passengers = [];

    flight.passengers.push(person);

    await flight.save();
  }
}
