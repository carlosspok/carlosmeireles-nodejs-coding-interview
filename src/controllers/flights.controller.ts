import { Body, Get, JsonController, Post } from 'routing-controllers';

import { AddPassengerDto } from './dto';
import { FlightsService } from '../services/flights.service';

const flightsService = new FlightsService()

@JsonController('/flights')
export default class FlightsController {
    @Get('', { transformResponse: false })
    async getAll() {
        const flights = await flightsService.getAll();

        return {
            status: 200,
            data: flights,
        }
    }

    @Post('/add-passenger')
    async addPassenger(@Body() addPassengerDto: AddPassengerDto) {
        await flightsService.addPassenger(addPassengerDto);

        return {
            status: 201
        }
    }
}
